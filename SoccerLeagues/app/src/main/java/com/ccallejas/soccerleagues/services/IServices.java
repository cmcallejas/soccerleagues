package com.ccallejas.soccerleagues.services;

import com.ccallejas.soccerleagues.models.ListEvents;
import com.ccallejas.soccerleagues.models.Listleagues;
import com.ccallejas.soccerleagues.models.TeamsList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IServices {

    @GET("all_leagues.php")
    Call<Listleagues> getLeagues();


    @GET("lookup_all_teams.php")
    Call<TeamsList> getListTeams(@Query("id") String idLeague);

    @GET("eventsnext.php")
    Call<ListEvents> getListaEventos(@Query("id") String idTeam);



}
