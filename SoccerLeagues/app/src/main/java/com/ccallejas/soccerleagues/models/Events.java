package com.ccallejas.soccerleagues.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class Events implements Serializable {

    @SerializedName("idEvent")
    @Expose
    private String idEvent;

    @SerializedName("strEvent")
    @Expose
    private String strEvent ;

    @SerializedName("strFilename")
    @Expose
    private String strFilename;

    @SerializedName("idLeague")
    @Expose
    private String idLeague ;

    @SerializedName("strLeague")
    @Expose
    private String strLeague ;

    @SerializedName("strTime")
    @Expose
    private String strTime ;

    @SerializedName("strDate")
    @Expose
    private String  strDate;

    @SerializedName("idHomeTeam")
    @Expose
    private String  idHomeTeam;

    @SerializedName("idAwayTeam")
    @Expose
    private String  idAwayTeam;


    public String getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(String idEvent) {
        this.idEvent = idEvent;
    }

    public String getStrEvent() {
        return strEvent;
    }

    public void setStrEvent(String strEvent) {
        this.strEvent = strEvent;
    }

    public String getIdLeague() {
        return idLeague;
    }

    public void setIdLeague(String idLeague) {
        this.idLeague = idLeague;
    }

    public String getStrLeague() {
        return strLeague;
    }

    public void setStrLeague(String strLeague) {
        this.strLeague = strLeague;
    }

    public String getStrTime() {
        return strTime;
    }

    public void setStrTime(String strTime) {
        this.strTime = strTime;
    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public String getIdHomeTeam() {
        return idHomeTeam;
    }

    public void setIdHomeTeam(String idHomeTeam) {
        this.idHomeTeam = idHomeTeam;
    }

    public String getIdAwayTeam() {
        return idAwayTeam;
    }

    public void setIdAwayTeam(String idAwayTeam) {
        this.idAwayTeam = idAwayTeam;
    }

    public String getStrFilename() {
        return strFilename;
    }

    public void setStrFilename(String strFilename) {
        this.strFilename = strFilename;
    }
}
