package com.ccallejas.soccerleagues.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class Listleagues implements Serializable {

    public ArrayList<Leagues> getListaLeagues() {
        return listaLeagues;
    }

    public void setListaLeagues(ArrayList<Leagues> listaLeagues) {
        this.listaLeagues = listaLeagues;
    }

    @SerializedName("leagues")
    @Expose
    private ArrayList<Leagues> listaLeagues;
}
