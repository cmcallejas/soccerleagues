package com.ccallejas.soccerleagues.views.interfaces;

public interface IBaseView {

     void showToast(int result);

}
