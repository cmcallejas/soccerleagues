package com.ccallejas.soccerleagues.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.models.Leagues;
import com.ccallejas.soccerleagues.views.interfaces.ILeaguesView;
import java.util.ArrayList;
import java.util.Locale;

public class AdapterLeagues  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Leagues> listaLeagues;
    private ILeaguesView miActivityView;
    public ArrayList<Leagues> arrayListleages;

    public AdapterLeagues(ArrayList<Leagues> leages, ILeaguesView iActivityView) {
        this.listaLeagues = leages;
        arrayListleages = new ArrayList<>();
        arrayListleages.addAll(listaLeagues);
        this.miActivityView = iActivityView;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_leagues,
                viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterLeagues.CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;
        final Leagues leagues = listaLeagues.get(position);
        customViewHolder.tvStrLeague.setText(leagues.getStrLeague());
        if((!"".equals(leagues.getStrLeagueAlternate()) && null != leagues.getStrLeagueAlternate())) {
            customViewHolder.tvStrLeagueAlternate.setText(leagues.getStrLeagueAlternate());
            customViewHolder.tvStrLeagueAlternate.setVisibility(View.VISIBLE);
            customViewHolder.tvStrNomAlternate.setVisibility(View.VISIBLE);
        }else{
            customViewHolder.tvStrLeagueAlternate.setVisibility(View.INVISIBLE);
            customViewHolder.tvStrNomAlternate.setVisibility(View.INVISIBLE);
        }

        customViewHolder.tvStrSport.setText("Deporte: "+leagues.getStrSport());

        customViewHolder.cardViewLiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                miActivityView.intentToListTeam(leagues);

            }
        });
    }

    @Override
    public int getItemCount() {
        return listaLeagues.size();
    }

    public void filter(String chartText) {
        chartText = chartText.toLowerCase(Locale.getDefault());
        listaLeagues.clear();
        if (chartText.length() == 0) {
            listaLeagues.addAll(arrayListleages);
        } else {
            for (Leagues e : arrayListleages) {
                if (e.getStrLeague().toLowerCase(Locale.getDefault())
                        .contains(chartText)) {
                    listaLeagues.add(e);
                }
            }
        }
        notifyDataSetChanged();
    }


    private class CustomViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewLeague;
        private TextView tvStrLeague;
        private TextView tvStrLeagueAlternate;
        private TextView tvStrNomAlternate;
        private CardView cardViewLiga;
        private TextView tvStrSport;

        CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewLeague = itemView.findViewById(R.id.imageViewLeague);
            tvStrLeague = itemView.findViewById(R.id.tvStrLeague);
            tvStrLeagueAlternate = itemView.findViewById(R.id.tvStrLeagueAlternate);
            tvStrNomAlternate = itemView.findViewById(R.id.tvStrNomAlternate);
            tvStrSport = itemView.findViewById(R.id.tvStrSport);
            cardViewLiga = itemView.findViewById(R.id.cardViewLeagues);
        }

    }
}
