package com.ccallejas.soccerleagues.presenters;

import com.ccallejas.soccerleagues.models.TeamsList;
import com.ccallejas.soccerleagues.services.Repository;
import com.ccallejas.soccerleagues.views.interfaces.ITeamsView;
import java.io.IOException;

public class TeamsPresenter extends  BasePresenter <ITeamsView>{

    private Repository repository;

    public void getListTeams(String idLig) {
        if (getValidateInternet().isConnected()) {
            repository = new Repository();
            createThreadGetTeams(String.valueOf(idLig));
        } else {
            getView().showAlert(idLig);
       }

    }

    public void createThreadGetTeams(final String idLiga){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getTeams(idLiga);
            }
        });
        thread.start();
    }


    private void getTeams(String idLiga) {
        try {
            TeamsList listaTeams = repository.getEquipos(idLiga);
            getView().getTeams(listaTeams.getTeams());
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

}
