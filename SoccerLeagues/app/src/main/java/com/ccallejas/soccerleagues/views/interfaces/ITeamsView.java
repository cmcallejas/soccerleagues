package com.ccallejas.soccerleagues.views.interfaces;

import com.ccallejas.soccerleagues.models.Team;


import java.util.ArrayList;

public interface ITeamsView extends IBaseView{

    void getTeams(ArrayList<Team>  teamsList);

    void showAlert(String idLiga);

    void intentToListTeam(Team team);
}
