package com.ccallejas.soccerleagues.views.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.adapter.AdapterEvents;
import com.ccallejas.soccerleagues.helper.Constants;
import com.ccallejas.soccerleagues.models.Events;
import com.ccallejas.soccerleagues.models.Team;
import com.ccallejas.soccerleagues.presenters.EventsPresenter;
import com.ccallejas.soccerleagues.views.FireMissilesDialogFragment;
import com.ccallejas.soccerleagues.views.interfaces.IEventsView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class EventsActivity  extends BaseActivity<EventsPresenter> implements IEventsView {

    private Team team;
    private AdapterEvents adapterEvents;
    private RecyclerView recyclerViewEvents;
    private TextView tvStrNombreTeam;
    private TextView strAnioFundacion;
    private TextView txtstrManager;
    private TextView tvstrStadium;
    private TextView txtVstrUbicacionStadium;
    private TextView txtVstrStadiumCapacidad;
    private ImageView imageViewCamisa;
    private FloatingActionButton btnDescripcionteam;
    private FloatingActionButton btnFacebook;
    private FloatingActionButton btnTwitter;
    private FloatingActionButton btnYoutube;
    private FloatingActionButton btnWWW;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        recyclerViewEvents = findViewById(R.id.recyclerViewEventos);
        tvStrNombreTeam =findViewById(R.id.tvStrNombreTeam);
        strAnioFundacion =findViewById(R.id.strAnioFundacion);
        txtstrManager =findViewById(R.id.txtstrManager);
        tvstrStadium =findViewById(R.id.tvstrStadium);
        txtVstrUbicacionStadium =findViewById(R.id.txtVstrUbicacionStadium);
        txtVstrStadiumCapacidad =findViewById(R.id.txtVstrStadiumCapacidad);
        imageViewCamisa =findViewById(R.id.imageViewCamisa);
        btnDescripcionteam =findViewById(R.id.btnDescripcionteam);
        btnFacebook =findViewById(R.id.btnFacebook);
        btnTwitter =findViewById(R.id.btnTwitter);
        btnYoutube =findViewById(R.id.btnYoutube);
        btnWWW =findViewById(R.id.btnWWW);

        setPresenter(new EventsPresenter());
        getPresenter().inject(this, getValidateInternet());

        team = (Team) getIntent().getSerializableExtra(Constants.TEAM);
        getPresenter().getEventosTeam(team.getIdTeam());

        btnDescripcionteam.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FireMissilesDialogFragment dialogo = new FireMissilesDialogFragment()
                        .setMessage((!"".equals(team.getStrDescriptionES()) && (null != team.getStrDescriptionES()))?team.getStrDescriptionES():"Equipo sin descripción.");
                dialogo.show(fragmentManager, "tagAlerta");
            }
        });

        btnFacebook.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
               String msg =validateRedesSociales(team,"F");
                if("".equals(msg)){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://"+team.getStrFacebook())));
                }else{
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FireMissilesDialogFragment dialog = new FireMissilesDialogFragment()
                            .setMessage(msg);
                    dialog.show(fragmentManager, "tagAlerta");
                }

            }
        });

        btnTwitter.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                String msg =validateRedesSociales(team,"T");
                if("".equals(msg)){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://"+team.getStrTwitter())));
                }else{
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FireMissilesDialogFragment dialog = new FireMissilesDialogFragment()
                            .setMessage(msg);
                    dialog.show(fragmentManager, "tagAlerta");
                }

            }
        });

        btnYoutube.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                String msg =validateRedesSociales(team,"Y");
                if("".equals(msg)){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://"+team.getStrYoutube())));
                }else{
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FireMissilesDialogFragment dialog = new FireMissilesDialogFragment()
                            .setMessage(msg);
                    dialog.show(fragmentManager, "tagAlerta");
                }

            }
        });


        btnWWW.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                String msg =validateRedesSociales(team,"W");
                if("".equals(msg)){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://"+team.getStrWebsite())));
                }else{
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FireMissilesDialogFragment dialog = new FireMissilesDialogFragment()
                            .setMessage(msg);
                    dialog.show(fragmentManager, "tagAlerta");
                }

            }
        });
    }

    protected String validateRedesSociales(Team team, String sitio){
        String msg="";

        switch (sitio) {
            case "F":
                if("".equals(team.getStrFacebook()) || (null == team.getStrFacebook())){
                    msg = "El equipo "+team.getStrTeam()+" no tiene cuenta de Facebook.";
                }
                break;

            case "T":
                if("".equals(team.getStrTwitter()) || (null == team.getStrTwitter())){
                    msg = "El equipo "+team.getStrTeam()+" no tiene cuenta de Twitter.";
                }
                break;

            case"Y":
                if("".equals(team.getStrYoutube()) || (null == team.getStrYoutube())){
                    msg = "El equipo "+team.getStrTeam()+" no tiene cuenta de Youtube.";
                }
                break;

            case "W":
                if("".equals(team.getStrWebsite()) || (null == team.getStrWebsite())){
                    msg = "El equipo "+team.getStrTeam()+" no tiene sitio web.";
                }
                break;

           default:
               msg = "No se reconoce la red social ingresada.";
                break;
        }

        return msg;
    }

    @Override
    public void intentToListEventTeam(Events events) {

    }

    @Override
    public void getEventos(final ArrayList<Events> eventList) {
        runOnUiThread(new Runnable() {
            @SuppressLint("StringFormatInvalid")
            @Override
            public void run() {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(EventsActivity.this);
                recyclerViewEvents.setLayoutManager(linearLayoutManager);
                recyclerViewEvents.setHasFixedSize(true);
                adapterEvents = new AdapterEvents(eventList, team,EventsActivity.this);
                recyclerViewEvents.setAdapter(adapterEvents);

                tvStrNombreTeam.setText("Equipo: "+team.getStrTeam());
                strAnioFundacion.setText("Año: "+team.getIntFormedYear());
                txtstrManager.setText("Manager: "+team.getStrManager());
                tvstrStadium.setText(team.getStrStadium());
                txtVstrUbicacionStadium.setText("Ubicacion: "+team.getStrStadiumLocation());
                txtVstrStadiumCapacidad.setText("Capacidad: "+team.getIntStadiumCapacity());
                Picasso.get().load(team.getStrTeamJersey()).into(imageViewCamisa);
                imageViewCamisa.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void showAlert(final String idTeam) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.title_validate_internet);
        alertDialog.setMessage(R.string.message_validate_internet);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(R.string.text_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getPresenter().getEventosTeam(idTeam);
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();

    }

    public Team getTeam(){
        return team;
    }

}
