package com.ccallejas.soccerleagues.helper;

public class Constants {

    public static final String LISTA_LIGAS = "LISTA_LIGAS";
    public static final String LIGA = "LIGA";
    public static final String TEAM = "TEAM";
    public static final String EVENTOS = "EVENTOS";

}
