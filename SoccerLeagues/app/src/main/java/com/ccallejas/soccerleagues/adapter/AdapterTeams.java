package com.ccallejas.soccerleagues.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.models.Team;
import com.ccallejas.soccerleagues.views.interfaces.ITeamsView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Locale;

public class AdapterTeams extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Team> listaTeam;
    private ITeamsView miActivityView;
    public ArrayList<Team> arraylistTeam;

    public AdapterTeams(ArrayList<Team> listaTeam, ITeamsView miActivityView) {
        this.listaTeam = listaTeam;
        arraylistTeam = new ArrayList<>();
        arraylistTeam.addAll(listaTeam);
        this.miActivityView = miActivityView;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_teams,
                viewGroup, false);
        return new  AdapterTeams.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterTeams.CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;
        final Team team = listaTeam.get(position);
        customViewHolder.tvStrTeam.setText(team.getStrTeam());
        customViewHolder.tvstrStadium.setText(team.getStrStadium());
        if((!"".equals(team.getStrAlternate()) && null != team.getStrAlternate())) {
            customViewHolder.tvstrAlternate.setText(team.getStrAlternate());
            customViewHolder.tvstrAlternate.setVisibility(View.VISIBLE);
            customViewHolder.strLabelAlternate.setVisibility(View.VISIBLE);
        }else{
            customViewHolder.tvstrAlternate.setVisibility(View.INVISIBLE);
            customViewHolder.strLabelAlternate.setVisibility(View.INVISIBLE);
        }
        Picasso.get().load(team.getStrTeamBadge()).into(customViewHolder.imageStrTeamBadge);
        customViewHolder.cardViewTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(":::TEAM::: ","OBJETO TEAM :"+team);
                miActivityView.intentToListTeam(team);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (listaTeam !=null ?listaTeam.size():0);
    }

    public void filter(String chartText) {
        chartText = chartText.toLowerCase(Locale.getDefault());
        listaTeam.clear();
        if (chartText.length() == 0) {
            listaTeam.addAll(arraylistTeam);
        } else {
            for (Team e : arraylistTeam) {
                if (e.getStrTeam().toLowerCase(Locale.getDefault())
                        .contains(chartText)) {
                    listaTeam.add(e);
                }
            }
        }
        notifyDataSetChanged();
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageStrTeamBadge;
        private TextView tvstrAlternate;
        private TextView strLabelAlternate;
        private TextView tvStrTeam;
        private TextView tvstrStadium;
        private CardView cardViewTeam;

        CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            imageStrTeamBadge = itemView.findViewById(R.id.imageViewTeam);
            tvStrTeam = itemView.findViewById(R.id.tvStrTeam);
            tvstrStadium = itemView.findViewById(R.id.tvstrStadium);
            tvstrAlternate =itemView.findViewById(R.id.strAlternate);
            strLabelAlternate =itemView.findViewById(R.id.strLabelAlternate);
            cardViewTeam = itemView.findViewById(R.id.cardViewTeam);
        }

    }
}
