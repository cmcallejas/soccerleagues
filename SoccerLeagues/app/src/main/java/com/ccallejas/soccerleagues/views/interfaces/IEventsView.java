package com.ccallejas.soccerleagues.views.interfaces;

import com.ccallejas.soccerleagues.models.Events;

import java.util.ArrayList;


public interface IEventsView extends IBaseView{

    void intentToListEventTeam(Events events);

    void getEventos(ArrayList<Events> listEvent);

    void showAlert(String idteam);
}
